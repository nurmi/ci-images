FROM ubuntu:22.04

LABEL Description="KDE Static website generation image"
MAINTAINER Bhushan Shah

USER root

ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install --no-install-recommends --yes \
ruby ruby-dev \
build-essential \
libsqlite3-dev \
zlib1g-dev \
git git-lfs \
python3-pip \
python3-venv \
php8.1-cli php8.1-xml \
# fontforge is used by breeze-icons to generate the web icon font
fontforge \
locales \
golang-go \
doxygen graphviz qdoc-qt5 \
# Our Sphinx sites produce lots of duplicates for the different languages
# We use rdfind to eliminate duplicates and symlinks to make absolute symlinks relative
rdfind symlinks \
wget curl rsync pandoc subversion gettext \
# GCompris website generation:
# - Qt5 tools for lconvert and lrelease
# - htmlmin, PyQt5 and Jinja2 as Python dependencies
qttools5-dev-tools python3-pyqt5 python3-pyqt5.qtquick \
# gnupg is needed for apt-key
gnupg \
&& apt-get clean

RUN locale-gen en_US.UTF-8

RUN curl -fsSL https://deb.nodesource.com/setup_20.x | bash -
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install --yes yarn nodejs && apt-get clean

# Hugo needed by many of our static sites
RUN wget https://github.com/gohugoio/hugo/releases/download/v0.110.0/hugo_extended_0.110.0_linux-amd64.deb && \
	dpkg -i hugo_extended_0.110.0_linux-amd64.deb && rm hugo_extended_0.110.0_linux-amd64.deb

RUN gem install xdg:2.2.5 jekyll:3.8.4 jekyll-feed jekyll-theme-minimal jekyll-readme-index jekyll-relative-links jekyll-environment-variables pluto bundler haml redcarpet minima rdiscount inqlude addressable jekyll-kde-theme rouge

RUN pip3 install \
    requests pytx dateparser pelican mdx_video beautifulsoup4 polib python-frontmatter jieba python-gitlab feedparser \
    # Pelican has a number of plugins which Rolisteam is using that rely on functionality deprecated in Python Markdown 3.2 and which was ripped out in 3.4
    # Until they fix this, lock ourselves to 3.3 to ensure we have a version installed those plugins can work with. See https://phabricator.kde.org/T15881
    markdown==3.3 \
    # apps.kde.org generation
    py-appstream \
    # Build Notifier
    matrix-nio \
    # CI Tooling
    pyyaml lxml \
    # Paramiko to support interacting with the CI Notary Service
    paramiko \
    # Sphinx iteself and themes and extensions
    sphinx==7.2.6 sphinx-intl aether-sphinx sphinx_rtd_theme sphinxcontrib-doxylink sphinxcontrib-websupport \
    # Sphinx extensions for docs.kdenlive.org
    sphinxcontrib-video \
    # GCompris website generation
    htmlmin Jinja2

RUN wget https://getcomposer.org/download/1.9.0/composer.phar && mv composer.phar /usr/bin/composer && chmod +x /usr/bin/composer

RUN apt-get -qq update && apt-get install -qq \
    openssh-server \
    openjdk-8-jre-headless \
    && dpkg-reconfigure openssh-server && mkdir -p /var/run/sshd && apt-get -qq clean

# Translation framework for the Hugo sites
RUN git clone https://invent.kde.org/websites/hugo-i18n && pip3 install ./hugo-i18n

# Automated HTML tests used by many of our generated / static websites
RUN curl https://htmltest.wjdp.uk | bash -s -- -b /usr/local/bin

# Generation of api docs for api.kde.org
RUN git clone https://invent.kde.org/frameworks/kapidox.git /kapidox \
    && pip3 install --no-deps -r /kapidox/requirements.frozen.txt /kapidox

# Setup a user account for everything else to be done under
RUN useradd -d /home/user/ -u 1000 --user-group --create-home -G video user

# Switch to our unprivileged user account
USER user
